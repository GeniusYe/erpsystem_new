<?php
namespace Application\Util;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;

class ControllerUtil
{
	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('course-info/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	public static function checkAuthentication(ServiceLocatorInterface $serviceLocator)
	{
		$auth = $serviceLocator->get('auth');
		if ($auth->hasIdentity())
		{
			$auth_status = array('is_authenticated' => true);
				
			$storage = $auth->getStorage()->read();
				
			$auth_status['user'] = $storage['user'];

			return $auth_status;
		}
		else
			return array('is_authenticated' => false);
	}
}