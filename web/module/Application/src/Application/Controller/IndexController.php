<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		// $bonus_service = $this->getServiceLocator()->get('bonus_service');

		// //$bonus_service->calculate();
		
		// $bonus_list = $bonus_service->query_by_attendant_and_date(1, 2014, 1);
		// $attendant_bonus_list = $bonus_service->query_by_date(2014, 1);
		// return new ViewModel(array('bonus_list' => $bonus_list, 'attendant_bonus_list' => $attendant_bonus_list));

		return new ViewModel(array('bonus_list' => [], 'attendant_bonus_list' => []));
	}
}
