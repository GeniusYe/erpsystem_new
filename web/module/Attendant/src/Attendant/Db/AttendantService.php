<?php

namespace Attendant\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Attendant\Util\ErrorType; 
use Libraries\SharedLib\EncodingConv;

class AttendantService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_id($id)
	{
		$attendant_result = $this->db_adapter->query('EXEC SP_Warrant_Attendants_Read @id = ?', array($id));
        $attendant = $attendant_result->current();
		$attendant['name'] = EncodingConv::switch_encoding($attendant['name']);
		return $attendant;
	}
	
	public function query_all()
	{
		$attendant_list_result = $this->db_adapter->query('EXEC SP_Warrant_Attendants_GetDefaultOnes', array());
		$attendant_list = array();
		foreach ($attendant_list_result as $attendant)
		{
			$attendant['name'] = EncodingConv::switch_encoding($attendant['name']);
			array_push($attendant_list, $attendant);
		}
		return $attendant_list;
	}
	
	public function add_attendant($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Warrant_Attendants_Add @name = ?',
			array(EncodingConv::switch_encoding_reverse($parameter['name'])));
		$id = $result->current()[""];
		if ($id == -1)
		    return array('result' => false, 'reason' => array(ErrorType::ATTENDANT_NAME_OCCUPIED));
		else
			return array('result' => true, 'id' => $id);
	}
	
	public function edit_attendant($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Warrant_Attendants_Edit @id = ?, @name = ?',
			array($parameter['id'], EncodingConv::switch_encoding_reverse($parameter['name'])));
		return array('result' => true);
	}
}

?>