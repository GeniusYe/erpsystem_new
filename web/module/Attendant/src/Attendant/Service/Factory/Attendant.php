<?php

namespace Attendant\Service\Factory;

use Interop\Container\ContainerInterface;
use Attendant\Db\AttendantService;

class Attendant
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$attendant_service = new AttendantService($db_adapter);
		return $attendant_service;
	}
}

?>