<?php

namespace Attendant\Controller\Factory;

use Interop\Container\ContainerInterface;
use Attendant\Controller\MaintainController;

class MaintainControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new MaintainController(
            $container->get('auth'),
            $container->get('attendant_service'),
            $container->get('database_adapter'),
        );
    }
}