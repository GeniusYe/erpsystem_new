<?php

namespace Attendant\Controller\Factory;

use Interop\Container\ContainerInterface;
use Attendant\Controller\QueryController;

class QueryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new QueryController(
            $container->get('auth'),
            $container->get('attendant_service'),
        );
    }
}