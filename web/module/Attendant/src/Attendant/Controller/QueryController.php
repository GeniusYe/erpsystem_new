<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Attendant for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Attendant\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Attendant\Util\ErrorType;
use Attendant\Util\ControllerUtil;

class QueryController extends AbstractActionController
{
    private $auth;
    private $attendant_service;

    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
        \Attendant\Db\AttendantService $attendant_service,
    ) {
        $this->auth = $auth;
        $this->attendant_service = $attendant_service;
    }

    public function allAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['attendant_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('attendant/layout/layout')->setVariables(array('navbar_active' => 'attendant'));
		
        $attendant_list = $this->attendant_service->query_all();
        return array('attendant_list' => $attendant_list);
    }
    
    public function oneAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['attendant_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
        $id = $this->params('id');
        if (is_numeric($id))
        {
            $attendant = $this->attendant_service->query_by_id($id);
			$this->layout()->setTemplate('attendant/layout/layout')->setVariables(array('navbar_active' => 'customer'));
            return array('attendant' => $attendant);
        }
        else
        {
            /* Rendering */
        
            $empty = array(
                'id' => 'new',
                'name' => '',
                'vip_level' => '0',
            );
			$this->layout()->setTemplate('attendant/layout/layout')->setVariables(array('navbar_active' => 'customer'));
            return array(
                'attendant' => $empty,
            );
        }
    }
}
