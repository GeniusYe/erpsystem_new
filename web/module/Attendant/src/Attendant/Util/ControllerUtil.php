<?php
namespace Attendant\Util;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;

class ControllerUtil
{
	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('attendant/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	public static function checkAuthentication(\Laminas\Authentication\AuthenticationService $auth)
	{
		if ($auth->hasIdentity())
		{
			$auth_status = array('is_authenticated' => true);
				
			$storage = $auth->getStorage()->read();
				
			$auth_status['user'] = $storage['user'];
			$auth_status['attendant_operator'] = $storage['attendant_operator'];

			return $auth_status;
		}
		else
			return array('is_authenticated' => false);
	}
}