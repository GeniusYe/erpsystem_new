<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'Attendant\Controller\Query' => Attendant\Controller\QueryController::class,
			'Attendant\Controller\Maintain' => Attendant\Controller\MaintainController::class,
		),
		'factories' => array(
			Attendant\Controller\QueryController::class => Attendant\Controller\Factory\QueryControllerFactory::class,
			Attendant\Controller\MaintainController::class => Attendant\Controller\Factory\MaintainControllerFactory::class,
		)
	),
	'router' => array(
		'routes' => array(
			'attendant' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/attendant',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'Attendant\Controller',
						'controller'	=> 'QueryController',
						'action'		=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'	     => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'attendant_service' => Attendant\Service\Factory\Attendant::class,
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Attendant' => __DIR__ . '/../view',
		),
		'template_map' => include __DIR__ . '/../template_map.php',
	),
);
