<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'User\Controller\Log' => User\Controller\LogController::class,
		),
		'factories' => array(
			User\Controller\LogController::class => User\Controller\Factory\LogControllerFactory::class,
		)
	),
	'router' => array(
		'routes' => array(
			'authentication' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/authentication',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'User\Controller',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'		 => '[0-9]*'
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'auth' => 'User\Service\Factory\Authentication',
			'user_service' => User\Service\Factory\User::class,
		),
	),
	'view_manager' => array(
		'template_map' => include __DIR__  . '/../template_map.php',
		'template_path_stack' => array(
			'User' => __DIR__ . '/../view',
		),
		'strategies' => array(
			'ViewJsonStrategy',
		),
		'template_map' => include __DIR__ . '/../template_map.php',
	),
);
