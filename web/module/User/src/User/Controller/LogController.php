<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/User for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Util\ControllerUtil;

class LogController extends AbstractActionController
{
	private $auth;
	private $user_service;

	public function __construct(
		\Laminas\Authentication\AuthenticationService $auth,
		\User\Db\UserService $user_service,
	) {
		$this->auth = $auth;
		$this->user_service = $user_service;
	}

	public function inAction()
	{
		$this->layout()->setTemplate('user/layout/layout');
		
		if (!$this->getRequest()->isPost()) 
		{	
			return array();
		}
		
		$username = $this->params()->fromPost('username');
		$password = $this->params()->fromPost('password');
		
		$auth_adapter = $this->auth->getAdapter();
		
		// below we pass the username and the password to the authentication adapter for verification
		$auth_adapter->setIdentity($username);
		$auth_adapter->setCredential($password);

		// here we do the actual verification
		$result = $this->auth->authenticate();
		$is_valid = $result->isValid();
		
		if(!$is_valid)
		{
		    return array('error' => $result->getCode());
		}
		
		// upon successful validation the getIdentity method returns
		// the user entity for the provided credentials
		$user = $result->getIdentity();
		if ($user == null)
		{
		    return array('error' => $result->getCode());
		}
		
		$rights = $this->user_service->query_rights_by_id($user['ID']);
		$store_arr = array(
				'user' => array('ID' => $user['ID'], 'name' => $user['name']), 
				'attendant_operator' => $rights['attendant_operator'], 
				'customer_operator' => $rights['customer_operator'], 
				'bonus_operator' => $rights['bonus_operator'], 
				'bonus_admin' => $rights['bonus_admin'],
				'ticket_fault_operator' => $rights['ticket_fault_operator'],
				'ticket_fault_admin' => $rights['ticket_fault_admin'],
				'ticket_operator' => $rights['ticket_operator'],
				'ticket_admin' => $rights['ticket_admin'],
		        'draft_operator' => $rights['draft_operator'],
		);
		$this->auth->getStorage()->write($store_arr);

		$viewModel = new ViewModel();
		$viewModel->setTemplate('user\log\login_success');
	   	$viewModel->setVariables(array('name' => $user['name']));
		
		return $viewModel;
	}
	
	public function outAction()
	{
		$this->auth->clearIdentity();
		return $this->redirect()->toRoute('authentication/default', 
			array('controller' => 'log', 'action' => 'in'));
	}
	
	public function statusAction()
	{
		$auth_status = ControllerUtil::checkAuthentication($this->auth, array());
		$data = array('result' => true);
		if ($auth_status['is_authenticated'] === true)
		{
			$data['user'] = $auth_status['user'];
		}
		else 
			$data['user'] = null;
		
		return ControllerUtil::generateAjaxViewModel($this->getResponse(), $data);
	}
}
