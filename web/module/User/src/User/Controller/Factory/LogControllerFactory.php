<?php

namespace User\Controller\Factory;

use Interop\Container\ContainerInterface;
use User\Controller\LogController;

class LogControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new LogController(
            $container->get('auth'),
            $container->get('user_service'),
        );
    }
}