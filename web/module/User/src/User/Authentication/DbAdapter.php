<?php

namespace User\Authentication;

use Laminas\Authentication\Adapter\AbstractAdapter;
use Laminas\Authentication\Result;
use User\Db\UserService;

class DbAdapter extends AbstractAdapter
{
	protected $user_service;
	
	public function authenticate()
	{
		$identity = $this->identity;
		$password = $this->credential;
		
		$user = $this->user_service->query_by_username_and_password($identity, md5($password));
		
		if ($user == null)
			return new Result(Result::FAILURE_CREDENTIAL_INVALID, $identity);
		
		return new Result(Result::SUCCESS, $user);
	}
	
	public function set_user_service(UserService $user_service)
	{
		$this->user_service = $user_service;
	}
}