<?php

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationService;
use User\Authentication\DbAdapter;


class Authentication
{
	public function __invoke(ContainerInterface $container)
	{
		$user_service = $container->get('user_service');
		$auth_db_adapter = new DbAdapter();
		$auth_db_adapter->set_user_service($user_service);
		$service = new AuthenticationService();
		$service->setAdapter($auth_db_adapter);
		return $service;
	}
}