<?php

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Db\UserService;

class User
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$user_service = new UserService($db_adapter);
		return $user_service;
	}
}

?>