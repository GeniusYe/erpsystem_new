<?php

namespace User\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Libraries\SharedLib\EncodingConv;

class UserService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_username_and_password($username, $password)
	{
		$user_result = $this->db_adapter->query('EXEC SP_Identity_Read @type = 1, @username = ?, @password = ?', array($username, $password));
		$user = $user_result->current();
		if ($user == false) {
		    // try card 
		    $user_result = $this->db_adapter->query('EXEC SP_Identity_Read @type = 0, @cardInformation = ?', array($password));
		    $user = $user_result->current();
		    if ($user == false) {
		        return null;
		    }
		}
		$user['name'] = EncodingConv::switch_encoding($user['name']);
		return $user;
	}
	
	public function query_rights_by_id($id)
	{
		$rights_result = $this->db_adapter->query('EXEC dbo.[SP_ERPSystemNew_Rights_Read] @user_id = ?', array($id));
		$rights = $rights_result->current();
		return array(
				'customer_operator' => $rights['rCustomerOperator'] > 0 ? true : false,
				'attendant_operator' => $rights['rAttendantOperator'] > 0 ? true : false,
				'bonus_operator' => $rights['rBonusOperator'] > 0 ? true : false,
				'bonus_admin' => $rights['rBonusAdmin'] > 0 ? true : false,
				'ticket_operator' => $rights['rTicketOperator'] > 0 ? true : false,
				'ticket_admin' => $rights['rTicketAdmin'] > 0 ? true : false,
				'ticket_fault_operator' => $rights['rTicketFaultOperator'] > 0 ? true : false,
				'ticket_fault_admin' => $rights['rTicketFaultAdmin'] > 0 ? true : false,
		        'draft_operator' => $rights['rDraftOperator'] > 0 ? true : false,
		);
	}
}

?>