<?php
namespace Ticket\Util;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;

class ControllerUtil
{
	public static function dateCheck($id)
	{
		if ($id === null)
			return array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_GIVEN));
		$year = intval(substr($id, 0, 4));
		$month = intval(substr($id, 4, 2));
		
		$ret = array('result' => true, 'year' => $year, 'month' => $month, 'time_str' => $year . ($month >= 10 ? $month : "0$month"));
		
		return $ret;
	}
	
	public static function generateErrorViewModel($response, $variables)
	{
		$viewModel = new ViewModel();
		$viewModel->setTemplate('ticket/error');
		$viewModel->setVariables(array('result' => $variables));
		$viewModel->setTerminal(true);
	
		$response->setStatusCode(401);
		 
		return $viewModel;
	}
	
	public static function generateAjaxViewModel($response, $variables)
	{
		$jsonModel = new JsonModel($variables);

		if ($variables['result'] !== true)
			$response->setStatusCode(401);
		 
		return $jsonModel;
	}
	
	/**
	 * @param course_id
	 * @param auth
	 * @param is_authenticated
	 */
	public static function checkAuthentication(\Laminas\Authentication\AuthenticationService $auth)
	{
		if ($auth->hasIdentity())
		{
			$auth_status = array('is_authenticated' => true);
				
			$storage = $auth->getStorage()->read();
				
			$auth_status['user'] = $storage['user'];
			$auth_status['ticket_fault_operator'] = $storage['ticket_fault_operator'];
			$auth_status['ticket_fault_admin'] = $storage['ticket_fault_admin'];
			$auth_status['ticket_operator'] = $storage['ticket_operator'];
			$auth_status['ticket_admin'] = $storage['ticket_admin'];

			return $auth_status;
		}
		else
			return array('is_authenticated' => false);
	}
}