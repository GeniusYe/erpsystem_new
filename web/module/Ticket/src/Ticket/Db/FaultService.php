<?php

namespace Ticket\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Ticket\Util\ErrorType; 
use Libraries\SharedLib\EncodingConv;

class FaultService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_id($id)
	{
		$ticket_fault_result = $this->db_adapter->query('EXEC SP_Ticket_Fault_Read @id = ?', array($id));
        $ticket_fault = $ticket_fault_result->current();
		$ticket_fault['name'] = EncodingConv::switch_encoding($ticket_fault['name']);
		return $ticket_fault;
	}
	
	public function query_all()
	{
		$ticket_fault_list_result = $this->db_adapter->query('EXEC SP_Ticket_Fault_GetDefaultOnes', array());
		$ticket_fault_list = array();
		foreach ($ticket_fault_list_result as $ticket_fault)
		{
			$ticket_fault['name'] = EncodingConv::switch_encoding($ticket_fault['name']);
			array_push($ticket_fault_list, $ticket_fault);
		}
		return $ticket_fault_list;
	}
	
	public function add_ticket_fault($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Fault_Add @name = ?, @working_hour = ?',
			array(EncodingConv::switch_encoding_reverse($parameter['name']), $parameter['working_hour']));
		$id = $result->current()[""];
		if ($id == -1)
		    return array('result' => false, 'reason' => array(ErrorType::TICKET_FAULT_NAME_OCCUPIED));
		else
			return array('result' => true, 'id' => $id);
	}
	
	public function edit_ticket_fault($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Fault_Edit @id = ?, @name = ?, @working_hour = ?',
			array($parameter['id'], EncodingConv::switch_encoding_reverse($parameter['name']), $parameter['working_hour']));
		if ($result->current()[""] == -1)
		    return array('result' => false, 'reason' => array(ErrorType::TICKET_FAULT_NAME_OCCUPIED));
		else
			return array('result' => true);
	}
}

?>