<?php

namespace Ticket\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Ticket\Util\ErrorType;
use Libraries\SharedLib\EncodingConv;

class TicketService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_id($id)
	{
		$ticket_result = $this->db_adapter->query('EXEC SP_Ticket_Read @id = ?', array($id));
        $ticket = $ticket_result->current();
		$ticket['customer_name'] = EncodingConv::switch_encoding($ticket['customer_name']);
		$ticket['attendant_name'] = EncodingConv::switch_encoding($ticket['attendant_name']);
		$ticket['operator_name'] = EncodingConv::switch_encoding($ticket['operator_name']);
		$ticket['contact'] = EncodingConv::switch_encoding($ticket['contact']);
		$ticket['phone'] = EncodingConv::switch_encoding($ticket['phone']);
		$ticket['address'] = EncodingConv::switch_encoding($ticket['address']);
		$ticket['comment'] = EncodingConv::switch_encoding($ticket['comment']);
		$ticket['solved_fault'] = EncodingConv::switch_encoding($ticket['solved_fault']);
		$ticket['remaining_fault'] = EncodingConv::switch_encoding($ticket['remaining_fault']);
		$ticket['machine_model'] = EncodingConv::switch_encoding($ticket['machine_model']);
		$ticket['submit_date'] = date('Y-m-d', strtotime($ticket['submit_date']));
		if ($ticket['finish_date'] != null)
			$ticket['finish_date'] = date('Y-m-d', strtotime($ticket['finish_date']));
		return $ticket;
	}
	
	public function collect($parameter)
	{
		$ticket_list_result = $this->db_adapter->query(
			'EXEC SP_Ticket_Collect @startDate = ?, @stopDate = ?, @col_restrict = ?', 
			array(
				$parameter['startDate'],
				$parameter['endDate'],
				$parameter['col_restrict'],
			)
		);
		$ticket_list = array();
		foreach ($ticket_list_result as $ticket)
		{
			$ticket['name'] = EncodingConv::switch_encoding($ticket['name']);
			array_push($ticket_list, $ticket);
		}
		return $ticket_list;
	}
	
	public function query_all($parameter)
	{
		if (!empty($parameter['col_restrict']) && !empty($parameter['col_restrict_id']))
		{
			$ticket_list_result = $this->db_adapter->query(
				'EXEC SP_Ticket_GetDefaultOnes @startDate = ?, @stopDate = ?, @col_restrict = ?, @col_restrict_id = ?', 
				array(
					$parameter['startDate'],
					$parameter['endDate'],
					$parameter['col_restrict'],
					$parameter['col_restrict_id'],
				)
			);
		}
		else
		{
			$ticket_list_result = $this->db_adapter->query(
				'EXEC SP_Ticket_GetDefaultOnes @startDate = ?, @stopDate = ?',
				array(
					$parameter['startDate'],
					$parameter['endDate'],
				)
			);
		}
		$ticket_list = array();
		foreach ($ticket_list_result as $ticket)
		{
			$ticket['customer_name'] = EncodingConv::switch_encoding($ticket['customer_name']);
			$ticket['attendant_name'] = EncodingConv::switch_encoding($ticket['attendant_name']);
			array_push($ticket_list, $ticket);
		}
		return $ticket_list;
	}
	
	public function cancel_ticket($id)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Cancel @id = ?',
			array($id));
		$result_v = $result->current()[""];
		if ($result_v == 0)
			return array('result' => true);
		else if($result_v == -2)
			return array('result' => false, 'reason' => array(ErrorType::TICKET_TOO_OLD_TO_CANCEL));
		else 
			return array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID));
	}
	
	public function copy_ticket($id, $parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Copy @id = ?, @operator = ?',
			array($id, $parameter['operator']));
		$id = $result->current()[""];
		return array('result' => true, 'id' => $id);
	}
	
	public function add_ticket($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Add 
				@customer_id = ?, @attendant_id = ?, @operator = ?, @truck = ?, @distance = ?, @comment = ?,
				@contact = ?, @phone = ?, @address =?, @working_hour = ?, @solved_fault = ?, @machine_model = ?',
			array(
					$parameter['customer_id'],
					$parameter['attendant_id'],
					$parameter['operator'],
					$parameter['truck'],
					$parameter['distance'],
					EncodingConv::switch_encoding_reverse($parameter['comment']),
					EncodingConv::switch_encoding_reverse($parameter['contact']),
					EncodingConv::switch_encoding_reverse($parameter['phone']),
					EncodingConv::switch_encoding_reverse($parameter['address']),
					$parameter['working_hour'],
					EncodingConv::switch_encoding_reverse($parameter['solved_fault']),
					EncodingConv::switch_encoding_reverse($parameter['machine_model']),
			));
		$id = $result->current()[""];
		return array('result' => true, 'id' => $id);
	}
	
	public function edit_ticket_initial($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Edit_Initial 
				@id = ?, 
				@customer_id = ?, @attendant_id = ?, @operator = ?, @truck = ?, @distance = ?, @comment = ?,
				@contact = ?, @phone = ?, @address =?, @working_hour = ?, @solved_fault = ?, @machine_model = ?',
			array(
					$parameter['id'],
					$parameter['customer_id'],
					$parameter['attendant_id'],
					$parameter['operator'],
					$parameter['truck'],
					$parameter['distance'],
					EncodingConv::switch_encoding_reverse($parameter['comment']),
					EncodingConv::switch_encoding_reverse($parameter['contact']),
					EncodingConv::switch_encoding_reverse($parameter['phone']),
					EncodingConv::switch_encoding_reverse($parameter['address']),
					$parameter['working_hour'],
					EncodingConv::switch_encoding_reverse($parameter['solved_fault']),
					EncodingConv::switch_encoding_reverse($parameter['machine_model']),
			));
		return array('result' => true);
	}
	
	public function edit_ticket_finished($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Ticket_Edit_Finished
				@id = ?, 
				@comment = ?, @working_hour = ?, @solved_fault = ?, @remaining_fault = ?, @machine_model = ?',
			array(
					$parameter['id'],
					EncodingConv::switch_encoding_reverse($parameter['comment']),
					$parameter['working_hour'],
					EncodingConv::switch_encoding_reverse($parameter['solved_fault']),
					EncodingConv::switch_encoding_reverse($parameter['remaining_fault']),
					EncodingConv::switch_encoding_reverse($parameter['machine_model']),
			));
		return array('result' => true);
	}

	public function print_ticket($id)
	{
		$result = $this->db_adapter->query('EXEC SP_Ticket_Print @id = ?', array( $id ));
		return array('result' => true);
	}
}

?>