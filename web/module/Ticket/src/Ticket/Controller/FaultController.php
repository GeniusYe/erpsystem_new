<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Ticket for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ticket\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\InputFilter\Factory as FilterFactory;
use Ticket\Util\ErrorType;
use Ticket\Util\ControllerUtil;

class FaultController extends AbstractActionController
{
    private $auth;
	private $db_adapter;
	private $ticket_fault_service;

    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
		\Laminas\Db\Adapter\Adapter $db_adapter,
        \Ticket\Db\FaultService $ticket_fault_service,
    ) {
        $this->auth = $auth;
		$this->db_adapter = $db_adapter;
		$this->ticket_fault_service = $ticket_fault_service;
    }
	
    public function allAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_fault_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket_fault'));
		
        $ticket_fault_list = $this->ticket_fault_service->query_all();
        return array('ticket_fault_list' => $ticket_fault_list);
    }
    
    public function oneAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_fault_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
        $id = $this->params('id');
        if (is_numeric($id))
        {
            $ticket_fault = $this->ticket_fault_service->query_by_id($id);
			$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket_fault'));
            return array('ticket_fault' => $ticket_fault);
        }
        else
        {
            /* Rendering */
        
            $empty = array(
                'id' => 'new',
                'name' => '',
                'working_hour' => '0',
            );
			$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket_fault'));
            return array(
                'ticket_fault' => $empty,
            );
        }
    }
    
    public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'name' => array('filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'working_hour' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
		));

		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_fault_admin'])
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$this->db_adapter->getDriver()->getConnection()->beginTransaction();
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		
		if ($id === 'new') { }
		else if (is_numeric($id))
			$id = intval($id);
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Merge */
		
		if ($id === 'new')
		{
			$result = $this->ticket_fault_service->add_ticket_fault(array(
				'name' => $filter->getValue('name'),
				'working_hour' => $filter->getValue('working_hour') * 10,
			));

			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else
				$id = $result['id'];
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $this->ticket_fault_service->edit_ticket_fault(
				array(
				    'id' => $id,
					'name' => $filter->getValue('name'),
					'working_hour' => $filter->getValue('working_hour') * 10,
				)
			);
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$this->db_adapter->getDriver()->getConnection()->commit();

		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
}
