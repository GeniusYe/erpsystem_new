<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Ticket for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ticket\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\InputFilter\Factory as FilterFactory;
use Laminas\View\Model\ViewModel;
use Ticket\Util\ErrorType;
use Ticket\Util\ControllerUtil;

class MaintainController extends AbstractActionController
{
    private $auth;
	private $db_adapter;
    private $ticket_service;
	private $service_truck_list;

    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
		\Laminas\Db\Adapter\Adapter $db_adapter,
        \Ticket\Db\TicketService $ticket_service,
		$service_truck_list,
    ) {
        $this->auth = $auth;
		$this->db_adapter = $db_adapter;
		$this->ticket_service = $ticket_service;
		$this->service_truck_list = $service_truck_list;
    }

    public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'customer_id' => array( 'filters' => array( array( 'name' => 'int' ) ) ),
			'attendant_id' => array( 'filters' => array( array( 'name' => 'int' ) ) ),
			'contact' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'phone' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'address' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'truck' => array( 'filters' => array( array( 'name' => 'int' ) ) ),
			'distance' => array( 'required' => false, 'filters' => array( array( 'name' => 'int' ) ) ),
			'working_hour' => array(),
			'solved_fault' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'remaining_fault' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'machine_model' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'comment' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));

		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_operator'])
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$this->db_adapter->getDriver()->getConnection()->beginTransaction();
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		
		if ($id === 'new') { }
		else if (is_numeric($id))
			$id = intval($id);
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Merge */
		
		if ($id === 'new')
		{
			$result = $this->ticket_service->add_ticket(array(
				'customer_id' => $filter->getValue('customer_id'),
				'attendant_id' => $filter->getValue('attendant_id'),
				'operator' => $auth_status['user']['ID'],
				'contact' => $filter->getValue('contact'),
				'phone' => $filter->getValue('phone'),
				'address' => $filter->getValue('address'),
				'truck' => $filter->getValue('truck'),
				'distance' => $filter->getValue('distance'),
				'working_hour' => $filter->getValue('working_hour') * 10,
				'solved_fault' => $filter->getValue('solved_fault'),
				'machine_model' => $filter->getValue('machine_model'),
				'comment' => $filter->getValue('comment'),
			));

			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else
				$id = $result['id'];
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$ticket = $this->ticket_service->query_by_id($id);
			if ($ticket === false)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::TICKET_NOT_EXIST)));
			
			if ($ticket['state'] == 0)
			{
				$result = $this->ticket_service->edit_ticket_initial(
					array(
					    'id' => $id,
						'customer_id' => $filter->getValue('customer_id'),
						'attendant_id' => $filter->getValue('attendant_id'),
						'operator' => $auth_status['user']['ID'],
						'contact' => $filter->getValue('contact'),
						'phone' => $filter->getValue('phone'),
						'address' => $filter->getValue('address'),
						'truck' => $filter->getValue('truck'),
						'distance' => $filter->getValue('distance'),
						'working_hour' => $filter->getValue('working_hour') * 10,
						'solved_fault' => $filter->getValue('solved_fault'),
						'machine_model' => $filter->getValue('machine_model'),
						'comment' => $filter->getValue('comment'),
					)
				);
			}
			else if ($ticket['state'] == 1)
			{
				$result = $this->ticket_service->edit_ticket_finished(
					array(
						'id' => $id,
						'working_hour' => $filter->getValue('working_hour') * 10,
						'solved_fault' => $filter->getValue('solved_fault'),
						'remaining_fault' => $filter->getValue('remaining_fault'),
						'comment' => $filter->getValue('comment'),
						'machine_model' => $filter->getValue('machine_model'),
					)
				);
			}
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$this->db_adapter->getDriver()->getConnection()->commit();

		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
    
    public function copyAction()
    {
    	$auth_status = ControllerUtil::checkAuthentication($this->auth);
    	if ($auth_status['is_authenticated'] !== true)
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
    	if (!$auth_status['ticket_operator'])
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
    	
    	$id = $this->params('id');
    	if (!is_numeric($id))
    		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

    	$result = $this->ticket_service->copy_ticket($id, array('operator' => $auth_status['user']['ID']));
    	
    	$ret_result = array('result' => $result['result'], 'id' => $result['id']);
    	if ($result['result'] !== true)
    		$ret_result['reason'] = $result['reason'];
    	
		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), $ret_result);
    }
    
    public function cancelAction()
    {
    	$auth_status = ControllerUtil::checkAuthentication($this->auth);
    	if ($auth_status['is_authenticated'] !== true)
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
    	if (!$auth_status['ticket_operator'])
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
    	
    	$id = $this->params('id');
    	if (!is_numeric($id))
    		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));

    	$result = $this->ticket_service->cancel_ticket($id);
    	
    	$ret_result = array('result' => $result['result']);
    	if ($result['result'] !== true)
    		$ret_result['reason'] = $result['reason'];
    	
		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), $ret_result);
    }
    
    public function printAction()
    {
    	$auth_status = ControllerUtil::checkAuthentication($this->auth);
    	if ($auth_status['is_authenticated'] !== true)
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
    	if (!$auth_status['ticket_operator'])
    		return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
    	
    	$id = $this->params('id');
    	if (!is_numeric($id))
    		return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
    	
    	$ticket = $this->ticket_service->query_by_id($id);
    	$this->ticket_service->print_ticket($id);
    	
    	$viewModel = new ViewModel();
    	$viewModel->setVariables(array('ticket' => $ticket, 'service_truck_list' => $this->service_truck_list));
    	$viewModel->setTerminal(true);
    	return $viewModel;
    }
}
