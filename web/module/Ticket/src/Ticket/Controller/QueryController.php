<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Ticket for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ticket\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Ticket\Util\ErrorType;
use Ticket\Util\ControllerUtil;

class QueryController extends AbstractActionController
{
    private $auth;
    private $ticket_service;
	private $ticket_fault_service;
    private $customer_service;
	private $attendant_service;
	private $service_truck_list;

    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
        \Ticket\Db\TicketService $ticket_service,
        \Ticket\Db\FaultService $ticket_fault_service,
        \Customer\Db\CustomerService $customer_service,
        \Attendant\Db\AttendantService $attendant_service,
		$service_truck_list,
    ) {
        $this->auth = $auth;
		$this->ticket_service = $ticket_service;
		$this->ticket_fault_service = $ticket_fault_service;
        $this->customer_service = $customer_service;
		$this->attendant_service = $attendant_service;
		$this->service_truck_list = $service_truck_list;
    }

	public function indexAction()
	{
		$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket'));
		
		$customer_list = $this->customer_service->query_all();
		$attendant_list = $this->attendant_service->query_all();
		
		return array(
			'customer_list' => $customer_list,
			'attendant_list' => $attendant_list,
			'service_truck_list' => $this->service_truck_list
		);
	}
	
    public function collectAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket'));
		
        $startDate = null; $endDate = null; $col_restrict = null; $col_restrict_id = null;
		if (isset($_REQUEST['startDate'])) $startDate = $_REQUEST['startDate'];
		if (isset($_REQUEST['endDate'])) $endDate = $_REQUEST['endDate'];
		if (isset($_REQUEST['col_restrict']))
		{
			switch($_REQUEST['col_restrict'])
			{
				case 'customer_id':
					$col_restrict = 1;
					break;
				case 'attendant_id':
					$col_restrict = 2;
					break;
				case 'truck':
					$col_restrict = 3;
					break;
			}
		}
		
		$ticket_list = $this->ticket_service->collect(array('startDate' => $startDate, 'endDate' => $endDate, 'col_restrict' => $col_restrict));
		
        return array(
        	'ticket_list' => $ticket_list, 
        	'col_restrict' => $col_restrict,
        	'startDate' => $startDate,
        	'endDate' => $endDate,
        	'service_truck_list' => $this->service_truck_list
        );
    }
	
    public function allAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket'));
		
        $startDate = null; $endDate = null; $col_restrict = null; $col_restrict_id = null;
		if (isset($_REQUEST['startDate'])) $startDate = $_REQUEST['startDate'];
		if (isset($_REQUEST['endDate'])) $endDate = $_REQUEST['endDate'];
		if (isset($_REQUEST['col_restrict'])) $col_restrict = $_REQUEST['col_restrict'];
		if (isset($_REQUEST['col_restrict_id'])) $col_restrict_id = $_REQUEST['col_restrict_id'];
			
		$ticket_list = $this->ticket_service->query_all(array('startDate' => $startDate, 'endDate' => $endDate, 'col_restrict' => $col_restrict, 'col_restrict_id' => $col_restrict_id));
		
        return array('ticket_list' => $ticket_list, 'service_truck_list' => $this->service_truck_list);
    }
    
    public function oneAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['ticket_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$customer_list = $this->customer_service->query_all();
		$attendant_list = $this->attendant_service->query_all();
		$ticket_fault_list = $this->ticket_fault_service->query_all();
		
        $id = $this->params('id');
        if (is_numeric($id))
        {
            $ticket = $this->ticket_service->query_by_id($id);
			$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket'));
            return array(
            	'ticket' => $ticket, 
            	'customer_list' => $customer_list, 
            	'attendant_list' => $attendant_list, 
            	'service_truck_list' => $this->service_truck_list, 
            	'ticket_fault_list' => $ticket_fault_list,
            );
        }
        else
        {
            /* Rendering */
        
            $empty = array(
                'id' => 'new',
            	'state' => 0,
                'customer_id' => null,
            	'customer_name' => '',
                'attendant_id' => null,
            	'operator_name' => null,
            	'contact' => '',
            	'phone' => '',
            	'address' => '',
            	'truck' => 1,
            	'distance' => 0,
            	'working_hour' => 0,
				'solved_fault' => '-',
				'remaining_fault' => '-',
            	'submit_date' => '',
            	'comment' => '严禁井下、高空、高温及其他高危环境下作业',
            	'machine_model' => '',
            );
			$this->layout()->setTemplate('ticket/layout/layout')->setVariables(array('navbar_active' => 'ticket'));
            return array(
                'ticket' => $empty,
            	'customer_list' => $customer_list,
            	'attendant_list' => $attendant_list,
            	'service_truck_list' => $this->service_truck_list,
            	'ticket_fault_list' => $ticket_fault_list,
            );
        }
    }
}
