<?php

namespace Ticket\Controller\Factory;

use Interop\Container\ContainerInterface;
use Ticket\Controller\MaintainController;

class MaintainControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new MaintainController(
            $container->get('auth'),
            $container->get('database_adapter'),
            $container->get('ticket_service'),
            $container->get('config')['service_truck_list'],
        );
    }
}