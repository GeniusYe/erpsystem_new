<?php

namespace Ticket\Controller\Factory;

use Interop\Container\ContainerInterface;
use Ticket\Controller\FaultController;

class FaultControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new FaultController(
            $container->get('auth'),
            $container->get('database_adapter'),
            $container->get('ticket_fault_service'),
        );
    }
}