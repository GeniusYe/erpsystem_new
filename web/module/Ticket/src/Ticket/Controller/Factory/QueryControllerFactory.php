<?php

namespace Ticket\Controller\Factory;

use Interop\Container\ContainerInterface;
use Ticket\Controller\QueryController;

class QueryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new QueryController(
            $container->get('auth'),
            $container->get('ticket_service'),
            $container->get('ticket_fault_service'),
            $container->get('customer_service'),
            $container->get('attendant_service'),
            $container->get('config')['service_truck_list'],
        );
    }
}