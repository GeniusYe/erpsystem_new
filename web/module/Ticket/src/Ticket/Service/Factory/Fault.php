<?php

namespace Ticket\Service\Factory;

use Interop\Container\ContainerInterface;
use Ticket\Db\FaultService;

class Fault
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$ticket_fault_service = new FaultService($db_adapter);
		return $ticket_fault_service;
	}
}

?>