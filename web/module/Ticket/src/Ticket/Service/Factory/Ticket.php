<?php

namespace Ticket\Service\Factory;

use Interop\Container\ContainerInterface;
use Ticket\Db\TicketService;

class Ticket
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$ticket_service = new TicketService($db_adapter);
		return $ticket_service;
	}
}

?>