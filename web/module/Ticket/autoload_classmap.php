<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'Ticket\Module'                        => __DIR__ . '/Module.php',
  'Ticket\Controller\FaultController'    => __DIR__ . '/src/Ticket/Controller/FaultController.php',
  'Ticket\Controller\MaintainController' => __DIR__ . '/src/Ticket/Controller/MaintainController.php',
  'Ticket\Controller\QueryController'    => __DIR__ . '/src/Ticket/Controller/QueryController.php',
  'Ticket\Db\FaultService'               => __DIR__ . '/src/Ticket/Db/FaultService.php',
  'Ticket\Db\TicketService'              => __DIR__ . '/src/Ticket/Db/TicketService.php',
  'Ticket\Service\Factory\Fault'         => __DIR__ . '/src/Ticket/Service/Factory/Fault.php',
  'Ticket\Service\Factory\Ticket'        => __DIR__ . '/src/Ticket/Service/Factory/Ticket.php',
  'Ticket\Util\ControllerUtil'           => __DIR__ . '/src/Ticket/Util/ControllerUtil.php',
  'Ticket\Util\ErrorType'                => __DIR__ . '/src/Ticket/Util/ErrorType.php',
);
