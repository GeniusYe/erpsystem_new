<?php
return array(
	'service_truck_list' => array(1 => '无车辆'),
	'controllers' => array(
		'invokables' => array(
			'Ticket\Controller\Fault' => Ticket\Controller\FaultController::class,
			'Ticket\Controller\Query' => Ticket\Controller\QueryController::class,
			'Ticket\Controller\Maintain' => Ticket\Controller\MaintainController::class,
		),
		'factories' => array(
			Ticket\Controller\FaultController::class => Ticket\Controller\Factory\FaultControllerFactory::class,
			Ticket\Controller\QueryController::class => Ticket\Controller\Factory\QueryControllerFactory::class,
			Ticket\Controller\MaintainController::class => Ticket\Controller\Factory\MaintainControllerFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'ticket' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/ticket',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'Ticket\Controller',
						'controller'	=> 'QueryController',
						'action'		=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'	     => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'ticket_fault_service' => Ticket\Service\Factory\Fault::class,
			'ticket_service' => Ticket\Service\Factory\Ticket::class,
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Ticket' => __DIR__ . '/../view',
		),
		'template_map' => include __DIR__ . '/../template_map.php',
	),
);
