<?php

namespace Draft\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Libraries\SharedLib\EncodingConv;

class DraftService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	private function processWarrantList($out_warrant_list) {
	    $warrant_list = array();
	    foreach ($out_warrant_list as $warrant)
	    {
	        $warrant['target_name'] = EncodingConv::switch_encoding($warrant['target_name']);
	        $warrant['attendant_names'] = EncodingConv::switch_encoding($warrant['attendant_names']);
	        array_push($warrant_list, $warrant);
	    }
	    return $warrant_list;
	}
	
	public function queryOutWarrantList($parameters) {
	    $out_warrant_list = $this->db_adapter->query(
	        'EXEC SP_Warrant_Out_ViewOneCycle @date = ?',
            array(
                $parameters['date'],
            )
        );
        return self::processWarrantList($out_warrant_list);
	}
	
	public function queryOutWarrantListById($parameters) {
	    $out_warrant_list = $this->db_adapter->query(
	        'EXEC SP_Warrant_Out_ByID @id_list = ?',
            array(
                $parameters['id_list'],
            )
        );
        return self::processWarrantList($out_warrant_list);
	}
	
	private function processSoldDrafts($raw_draft_list) {
	    $draft_list = array();
	    foreach ($raw_draft_list as $draft)
	    {
	        $draft['name'] = EncodingConv::switch_encoding($draft['name']);
	        $draft['specification'] = EncodingConv::switch_encoding($draft['specification']);
	        $draft['hiddenSpecification'] = EncodingConv::switch_encoding($draft['hiddenSpecification']);
	        $draft['unit'] = EncodingConv::switch_encoding($draft['unit']);
	        array_push($draft_list, $draft);
	    }
	    return $draft_list;
	}
	
	public function querySoldDrafts($parameters) {
	    $raw_draft_list = $this->db_adapter->query(
	        'EXEC SP_Draft_Query_Total_Sale @startDate = ?, @stopDate = ?',
            array(
                $parameters['startDate'],
                $parameters['endDate'],
            )
        );
        
        return self::processSoldDrafts($raw_draft_list);
    }
	
	public function querySoldDraftsById($parameters) {
	    $raw_draft_list = $this->db_adapter->query(
	        'EXEC SP_Draft_Query_Total_Sale_By_ID @id_list = ?',
            array(
                $parameters['id_list'],
            )
        );
	    
        return self::processSoldDrafts($raw_draft_list);
    }
}

?>