<?php

namespace Draft\Service\Factory;

use Interop\Container\ContainerInterface;
use Draft\Db\DraftService;

class DraftServiceFactory
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$draft_service = new DraftService($db_adapter);
		return $draft_service;
	}
}

?>