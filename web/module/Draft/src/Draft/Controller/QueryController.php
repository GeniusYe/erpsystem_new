<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Draft for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Draft\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Draft\Util\ErrorType;
use Draft\Util\ControllerUtil;

class QueryController extends AbstractActionController
{
    private $auth;
    private $draft_service;
    
    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
        \Draft\Db\DraftService $draft_service,
    ) {
        $this->auth = $auth;
        $this->draft_service = $draft_service;
    }

    public function indexAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['draft_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('draft/layout/layout')->setVariables(array('navbar_active' => 'draft'));
	
        return array();
    }
    
    public function warrantListAction() {
        $auth_status = ControllerUtil::checkAuthentication($this->auth);
        if ($auth_status['is_authenticated'] !== true)
            return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
        if (!$auth_status['draft_operator'])
            return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
        
        $this->layout()->setTemplate('draft/layout/layout')->setVariables(array('navbar_active' => 'draft'));
        
        // index page send over only the month (2020-01), but we need a day (2020-01-01)
        if (isset($_REQUEST['date'])) $date = $_REQUEST['date']."-1";
        
        $out_warrant_list = $this->draft_service->queryOutWarrantList(array('date' => $date));
        
        return array('out_warrant_list' => $out_warrant_list);
    }
    
    public function queryAction()
    {
        $auth_status = ControllerUtil::checkAuthentication($this->auth);
        if ($auth_status['is_authenticated'] !== true)
            return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
        if (!$auth_status['draft_operator'])
            return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
        
        $this->layout()->setTemplate('draft/layout/layout')->setVariables(array('navbar_active' => 'draft'));

        if (isset($_REQUEST['id_list']))
        {
            $id_list = $_REQUEST['id_list'];
            $draft_sold_list = $this->draft_service->querySoldDraftsByID(array('id_list' => $id_list));
            $warrant_list = $this->draft_service->queryOutWarrantListById(array('id_list' => $id_list));
            
            return array('draft_sold_list' => $draft_sold_list, 'warrant_list' => $warrant_list, 'id_list' => $id_list);
        }
        else if (isset($_REQUEST['startDate']) && isset($_REQUEST['endDate']))
        {
            $startDate = $_REQUEST['startDate'];
            $endDate = $_REQUEST['endDate'];
            
            $draft_sold_list = $this->draft_service->querySoldDrafts(array('startDate' => $startDate, 'endDate' => $endDate));
            
            return array('draft_sold_list' => $draft_sold_list);
        }
        else
        {
            return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array('Either startDate vs endDate or id list must be provided')));
        }
        
        
    }
}
