<?php

namespace Draft\Controller\Factory;

use Interop\Container\ContainerInterface;
use Draft\Controller\QueryController;

class QueryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new QueryController(
            $container->get('auth'),
            $container->get('draft_service'),
        );
    }
}