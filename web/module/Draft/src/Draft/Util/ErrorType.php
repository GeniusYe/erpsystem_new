<?php
namespace Draft\Util;

class ErrorType
{
	const NOT_AUTHENTICATED = 'NOT_AUTHENTICATED';
	const NOT_ENOUGH_RIGHTS = 'NOT_ENOUGH_RIGHTS';
}