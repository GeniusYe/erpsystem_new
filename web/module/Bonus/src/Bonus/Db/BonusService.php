<?php

namespace Bonus\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Bonus\Util\ErrorType;

class BonusService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_date($year, $month)
	{
		$bonus_result = $this->db_adapter->query('EXEC SP_Warrant_Bonus_GetByDate @year = ?, @month = ?', array($year, $month));
		$bonus_list = array();
		foreach ($bonus_result as $item)
		{
			$new_item = $item;
			$new_item['attendant_name'] = $this->switch_encoding($new_item['attendant_name']);
			array_push($bonus_list, $new_item);
		}
		return $bonus_list;
	}
	
	public function query_by_attendant_and_date($attendant_id, $year, $month)
	{
		$bonus_list = array();
		
		$bonus_result = $this->db_adapter->query('EXEC SP_Warrant_Bonus_GetByAttendantAndDate @attendant_id = ?, @year = ?, @month = ?', array($attendant_id, $year, $month));
		$bonus_list_warrant = array();
		foreach ($bonus_result as $item)
		{
			$new_item = $item;
			$new_item['customer_name'] = $this->switch_encoding($new_item['customer_name']);
			array_push($bonus_list_warrant, $item);
		}
		$bonus_list['warrant'] = $bonus_list_warrant;
		
		$bonus_result = $this->db_adapter->query('EXEC SP_Warrant_Bonus_Addition_GetByAttendantAndDate @attendant_id = ?, @year = ?, @month = ?', array($attendant_id, $year, $month));
		$bonus_list_addition = array();
		foreach ($bonus_result as $item)
		{
			$new_item = $item;
			$new_item['reason'] = $this->switch_encoding($new_item['reason']);
			array_push($bonus_list_addition, $item);
		}
		$bonus_list['addition'] = $bonus_list_addition;
		
		return $bonus_list;
	}
	
	public function query_ratio_by_date($year, $month)
	{
		$ratio_result = $this->db_adapter->query('EXEC SP_Warrant_BonusRatio_Read @year = ?, @month = ?', array($year, $month));
		$ratio_list = array();
		foreach ($ratio_result as $item)
			$ratio_list[$item['type']] = $item;
		return $ratio_list;
	}
	
	public function set_ratio($year, $month, $ratio)
	{
		$cur_time = strtotime(date('Y-m', strtotime(date('Y-m-d')) - 86400 * 15));
		$reqeust_time = strtotime($year . '-' . $month . '-01');
		if ($reqeust_time < $cur_time)
		    return array('result' => false, 'reason' => array(ErrorType::OLD_RATIO_CANNOT_BE_CHANGED));
		    
	    if ($ratio['type'] == 2)
	    {
    	    $ratio_result = $this->db_adapter->query('EXEC SP_Warrant_BonusRatio_Set @year = ?, @month = ?, @type = ?, @draft_imme = ?, @draft_normal = ?, @draft_penalty = ?, @servicefee_imme = ?, @servicefee_normal = ?, @servicefee_penalty = ?',
    	        array($year, $month, $ratio['type'], $ratio['draft_imme'], $ratio['draft_normal'], $ratio['draft_penalty'], $ratio['servicefee_imme'], $ratio['servicefee_normal'], $ratio['servicefee_penalty']));
	    }
	    else if ($ratio['type'] == 0 || $ratio['type'] == 1)
	    {
	        $ratio_result = $this->db_adapter->query('EXEC SP_Warrant_BonusRatio_Set @year = ?, @month = ?, @type = ?, @draft_normal = ?, @servicefee_normal = ?',
	            array($year, $month, $ratio['type'], $ratio['draft_normal'], $ratio['servicefee_normal']));
	    }
	    
	    return array('result' => true);
	}
	
	public function query_addition_by_id($id)
	{
		$attendant_result = $this->db_adapter->query('EXEC SP_Warrant_Bonus_Addition_Read @id = ?', array($id));
        $attendant = $attendant_result->current();
		$attendant['reason'] = $this->switch_encoding($attendant['reason']);
		return $attendant;
	}
	
	public function add_addition($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Warrant_Bonus_Addition_Add @year = ?, @month = ?, @attendant_id = ?, @bonus = ?, @reason = ?',
			array($parameter['year'], $parameter['month'], $parameter['attendant_id'], $parameter['bonus'], $this->switch_encoding_reverse($parameter['reason'])));
		$id = $result->current()[""];
		return array('result' => true, 'id' => $id);
	}
	
	public function edit_addition($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Warrant_Bonus_Addition_Edit @id = ?, @year = ?, @month = ?, @attendant_id = ?, @bonus = ?, @reason = ?',
			array($parameter['id'], $parameter['year'], $parameter['month'], $parameter['attendant_id'], $parameter['bonus'], $this->switch_encoding_reverse($parameter['reason'])));
		return array('result' => true);
	}
	
	public function calculate($year, $month)
	{
		$cur_time = strtotime(date('Y-m', strtotime(date('Y-m-d')) - 86400 * 28));
		$cur_year = intval(date('Y', $cur_time));
		$cur_month = intval(date('m', $cur_time));
		if ($cur_month != $month || $cur_year != $year)
			return array('result' => false, 'reason' => array(ErrorType::NOT_CALCULATABLE_MONTH));
		
		$this->db_adapter->getDriver()->getConnection()->beginTransaction();
		
		$this->db_adapter->query('EXEC dbo.SP_System_MonthlySave', array()); 
		
		$ratio_list = $this->query_ratio_by_date($year, $month);
		
		$bonus_list = $this->query_bonus_by_date_for_calculate($year, $month);
		foreach ($bonus_list as $bonus)
		{
			$ratio_draft = 0;
			$ratio_servicefee = 0;
			$ratio_index = -1;
			switch ($bonus['warrant_type'])
			{
				case 20:
					$ratio_index = 0;
					break;
				case 1:
				case 0:
					if ($bonus['vip_level'] > 0)
						$ratio_index = 1;
					else 
						$ratio_index = 2;
					break;
				default:
					$ratio_index = -1;
			}
			
			if ($ratio_index < 0)
			{
				$this->set_bonus_by_warrant_id($bonus['warrant_id'], $year, $month, 0, 0, $ratio_index);
				continue;
			}
			
			$ratio = $ratio_list[$ratio_index];
			
			if ($ratio_index >= 2)
			{
    			$tolerance_time = 10;
    			$repay_delay = $bonus['repay_delay'];
    			
    			if ($repay_delay > $tolerance_time)
    			{
    				$ratio_draft = max(0, $ratio['ratio_draft_normal'] - $ratio['ratio_draft_penalty'] * ($repay_delay - $tolerance_time));
    				$ratio_servicefee = max(0, $ratio['ratio_servicefee_normal'] - $ratio['ratio_servicefee_penalty'] * ($repay_delay - $tolerance_time));
    			}
    			else
    			{
    				$ratio_draft = $ratio['ratio_draft_imme'];
    				$ratio_servicefee = $ratio['ratio_servicefee_imme'];
    			}
    					
    			$bonus_draft = $bonus['amount_draft'] * $ratio_draft / 10000;
    			$bonus_servicefee = $bonus['amount_servicefee'] * $ratio_servicefee / 10000;
			}
			else
			{
				$ratio_draft = $ratio['ratio_draft_normal'];
				$ratio_servicefee = $ratio['ratio_servicefee_normal'];
    					
    			$bonus_draft = $bonus['amount_draft'] * $ratio_draft / 10000;
    			$bonus_servicefee = $bonus['amount_servicefee'] * $ratio_servicefee / 10000;
			}
    		$this->set_bonus_by_warrant_id($bonus['warrant_id'], $year, $month, intval($bonus_draft), intval($bonus_servicefee), $ratio_index);
		}
		
		return array('result' => true); 
	}
	
	private function query_bonus_by_date_for_calculate($year, $month)
	{
		$bonus_result = $this->db_adapter->query('EXEC SP_Warrant_Bonus_GetByDateForCalculation @year = ?, @month = ?', array($year, $month));
		$bonus_list = array();
		foreach ($bonus_result as $item)
			array_push($bonus_list, $item);
		return $bonus_list;
	}
	
	private function set_bonus_by_warrant_id($warrant_id, $year, $month, $bonus_draft, $bonus_servicefee, $calculation_type)
	{
		$this->db_adapter->query('EXEC SP_Warrant_Bonus_Value_Set @warrant_id = ?, @year = ?, @month = ?, @bonus_draft = ?, @bonus_servicefee = ?, @calculation_type = ?', 
			  array($warrant_id, $year, $month, $bonus_draft, $bonus_servicefee, $calculation_type));
	}
	
	private function switch_encoding($str)
	{
		return iconv('GBK', 'UTF-8', $str);
	}
	
	private function switch_encoding_reverse($str)
	{
		return iconv('UTF-8', 'GBK', $str);
	}
}

?>