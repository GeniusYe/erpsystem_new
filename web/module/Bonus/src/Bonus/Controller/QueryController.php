<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Bonus\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Bonus\Util\ControllerUtil;
use Bonus\Util\ErrorType;

class QueryController extends AbstractActionController
{
	public function indexAction()
	{
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		return;
	}
	
	public function allAction()
	{
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$par_time = $this->params('id');
		if (empty($par_time))
		    return $this->redirect()->toRoute('bonus/default', array('controller' => 'index', 'action' => 'index', 'id' => ''));
		
		$date = ControllerUtil::dateCheck($par_time, true);
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => $date['reason']));

		$bonus_service = $this->getServiceLocator()->get('bonus_service');
	
		$bonus_list = $bonus_service->query_by_date($date['year'], $date['month']);
	
		$viewModel = new ViewModel();
		$viewModel->setTemplate('bonus/query/all');
		$viewModel->setVariables(array('time_str' => $date['time_str'],'bonus_list' => $bonus_list));
	
		return $viewModel;
	}
	
	public function oneAction()
	{
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$date = ControllerUtil::dateCheck($this->params('id'), true);
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), $date['reason']);
		
		$attendant_id = $this->params('id2');
		if ($attendant_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));

		$bonus_service = $this->getServiceLocator()->get('bonus_service');
		$attendant_service = $this->getServiceLocator()->get('attendant_service');
		
		$attendant = $attendant_service->query_by_id($attendant_id);
		
		$bonus_list = $bonus_service->query_by_attendant_and_date($attendant_id, $date['year'], $date['month']);
		
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$viewModel = new ViewModel();
		$viewModel->setTemplate('bonus/query/one');
		$viewModel->setVariables(array('bonus_list' => $bonus_list, 'time_str' => $date['time_str'], 'attendant' => $attendant, 'editable' => $date['editable']));
		
		return $viewModel;
	}
}
