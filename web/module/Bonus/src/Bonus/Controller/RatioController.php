<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Bonus\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\InputFilter\Factory as FilterFactory;
use Laminas\View\Model\ViewModel;
use Bonus\Util\ControllerUtil;
use Bonus\Util\ErrorType;

class RatioController extends AbstractActionController
{
	public function indexAction()
	{
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		return;
	}
	
	public function queryAction()
	{
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$par_time = $this->params('id');
		if ($par_time === null)
			return $this->redirect()->toRoute('bonus/default', array('controller' => 'index', 'action' => 'index', 'id' => ''));
		
		$date = ControllerUtil::dateCheck($par_time, true);
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => $date['reason']));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$bonus_service = $this->getServiceLocator()->get('bonus_service');
		
		$ratio_list = $bonus_service->query_ratio_by_date($date['year'], $date['month']);
		if (!isset($ratio_list[0]))
            $ratio_list[0] = array('ratio_draft_normal' => 0, 'ratio_servicefee_normal' => 0);
		if (!isset($ratio_list[1]))
            $ratio_list[1] = array('ratio_draft_normal' => 0, 'ratio_servicefee_normal' => 0);
		if (!isset($ratio_list[2]))
            $ratio_list[2] = array('ratio_draft_imme' => 0, 'ratio_draft_normal' => 0, 'ratio_draft_penalty' => 0, 'ratio_servicefee_imme' => 0, 'ratio_servicefee_normal' => 0, 'ratio_servicefee_penalty' => 0);

		$viewModel = new ViewModel();
		$viewModel->setTemplate('bonus/ratio/query');
		$viewModel->setVariables(array('time_str' => $date['time_str'], 'ratio_list' => $ratio_list, 'editable' => $date['editable']));
		
		return $viewModel;
	}
	
	public function setAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));

		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			't_service_ratio_draft' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_service_ratio_servicefee' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_vip_ratio_draft' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_vip_ratio_servicefee' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_draft_imme' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_draft_normal' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_draft_penalty' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_servicefee_imme' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_servicefee_normal' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
			't_normal_ratio_servicefee_penalty' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
		)); 
		
		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$date = ControllerUtil::dateCheck($this->params('id'), true);
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), $date['reason']);
		$year = $date['year'];
		$month = $date['month'];
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
	    
	    $bonus_service = $this->getServiceLocator()->get('bonus_service');
	    
	    $db_adapter = $this->getServiceLocator()->get('database_adapter');
	    $db_adapter->getDriver()->getConnection()->beginTransaction();
	    
	    /* 0 Service */
	    
	    $ratio = array();
	    $ratio['type'] = 0;
	    $ratio['draft_normal'] = $filter->getValue('t_service_ratio_draft') * 100;
	    $ratio['servicefee_normal'] = $filter->getValue('t_service_ratio_servicefee') * 100;
	    $ratio_list = $bonus_service->set_ratio($year, $month, $ratio);
	    
	    /* 1 VIP */
	    
	    $ratio = array();
	    $ratio['type'] = 1;
	    $ratio['draft_normal'] = $filter->getValue('t_vip_ratio_draft') * 100;
	    $ratio['servicefee_normal'] = $filter->getValue('t_vip_ratio_servicefee') * 100;
	    $ratio_list = $bonus_service->set_ratio($year, $month, $ratio);
	    
	    /* 2 Normal */
	    
	    $ratio = array();
	    $ratio['type'] = 2;
	    $ratio['draft_imme'] = $filter->getValue('t_normal_ratio_draft_imme') * 100;
	    $ratio['draft_normal'] = $filter->getValue('t_normal_ratio_draft_normal') * 100;
	    $ratio['draft_penalty'] = $filter->getValue('t_normal_ratio_draft_penalty') * 100;
	    $ratio['servicefee_imme'] = $filter->getValue('t_normal_ratio_servicefee_imme') * 100;
	    $ratio['servicefee_normal'] = $filter->getValue('t_normal_ratio_servicefee_normal') * 100;
	    $ratio['servicefee_penalty'] = $filter->getValue('t_normal_ratio_servicefee_penalty') * 100;
	    $ratio_list = $bonus_service->set_ratio($year, $month, $ratio);
	    
	    $db_adapter->getDriver()->getConnection()->commit();
	    
	    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true));
	}
}
