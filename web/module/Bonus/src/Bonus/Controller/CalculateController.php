<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link	  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Bonus\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Bonus\Util\ControllerUtil;
use Bonus\Util\ErrorType;

class CalculateController extends AbstractActionController
{
	public function indexAction()
	{
		$this->layout()->setTemplate('bonus/layout/layout')->setVariables(array('navbar_active' => 'bonus'));
		
		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$date = ControllerUtil::dateCheck($this->params('id'), true);
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => $date['reason']));
		
		$bonus_service = $this->getServiceLocator()->get('bonus_service');
		
		$calculate_result = $bonus_service->calculate($date['year'], $date['month']);
		if ($calculate_result['result'] === true)
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => true));
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => $calculate_result['reason']));
	}
}
