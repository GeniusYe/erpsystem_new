<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Attendant for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Bonus\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\InputFilter\Factory as FilterFactory;
use Bonus\Util\ErrorType;
use Bonus\Util\ControllerUtil;

class AdditionController extends AbstractActionController
{
    public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));

		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
				'id' => array(),
				'attendant_id' => array( 'required' => false, 'validators' => array( array( 'name' => 'int' ) ) ),
				'bonus' => array( 'validators' => array( array( 'name' => 'float' ) ) ),
				'reason' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));

		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID)));
		
		$date = ControllerUtil::dateCheck($this->params('id'));
		if ($date['result'] !== true)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), $date['reason']);
		
		$attendant_id = $this->params('id2');
		if ($attendant_id === null)
			return ControllerUtil::generateErrorViewModel($this->getResponse(), array('reason' => array(ErrorType::PARAMETER_NOT_GIVEN)));
		
		$raw_data = $this->getRequest()->getPost();

		$auth_status = ControllerUtil::checkAuthentication($this->getServiceLocator());
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['bonus_operator'] && !$auth_status['bonus_admin'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$db_adapter = $this->getServiceLocator()->get('database_adapter');
		$db_adapter->getDriver()->getConnection()->beginTransaction();

		$bonus_service = $this->getServiceLocator()->get('bonus_service');
		
		/* Start Query */
				
		$id = $raw_data['id'];
		
		if ($id === 'new') { }
		else if (is_numeric($id))
			$id = intval($id);
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Merge */
		
		if ($id === 'new')
		{
			$result = $bonus_service->add_addition(array(
				'attendant_id' => $attendant_id,
				'year' => $date['year'],
				'month' => $date['month'],
				'bonus' => $filter->getValue('bonus') * 100,
				'reason' => $filter->getValue('reason'),
			));

			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else
				$id = $result['id'];
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $bonus_service->edit_addition(
				array(
				    'id' => $id,
					'attendant_id' => $attendant_id,
					'year' => $date['year'],
					'month' => $date['month'],
					'bonus' => $filter->getValue('bonus') * 100,
					'reason' => $filter->getValue('reason'),
				)
			);
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$db_adapter->getDriver()->getConnection()->commit();

		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
}
