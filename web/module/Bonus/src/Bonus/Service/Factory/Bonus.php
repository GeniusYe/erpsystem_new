<?php

namespace Bonus\Service\Factory;

use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Bonus\Db\BonusService;

class Bonus implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$db_adapter = $serviceLocator->get('database_adapter');
		$bonus_service = new BonusService($db_adapter);
		return $bonus_service;
	}
}

?>