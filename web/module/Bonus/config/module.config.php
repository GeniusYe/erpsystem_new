<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'Bonus\Controller\Index' => 'Bonus\Controller\IndexController',
			'Bonus\Controller\Calculate' => 'Bonus\Controller\CalculateController',
			'Bonus\Controller\Addition' => 'Bonus\Controller\AdditionController',
			'Bonus\Controller\Query' => 'Bonus\Controller\QueryController',
			'Bonus\Controller\Ratio' => 'Bonus\Controller\RatioController',
		),
	),
	'router' => array(
		'routes' => array(
			'bonus' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/bonus',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'Bonus\Controller',
						'controller'	=> 'Query',
						'action'		=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id[/:id2]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'		 => '[0-9]*',
								'id2'		 => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'bonus_service' => 'Bonus\Service\Factory\Bonus',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Bonus' => __DIR__ . '/../view',
		),
		'template_map' => include __DIR__ . '/../template_map.php',
	),
);
