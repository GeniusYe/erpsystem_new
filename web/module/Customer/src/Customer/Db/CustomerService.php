<?php

namespace Customer\Db;

use Laminas\Db\Adapter\Adapter as DbAdapter;
use Customer\Util\ErrorType;
use Libraries\SharedLib\EncodingConv;

class CustomerService
{
	private $db_adapter;
	
	public function __construct(DbAdapter $adapter)
	{
		$this->db_adapter = $adapter;
	}
	
	public function query_by_id($id)
	{
		$customer_result = $this->db_adapter->query('EXEC SP_Customer_Read @id = ?', array($id));
        $customer = $customer_result->current();
		$customer['id'] = $customer['ID'];
		$customer['name'] = EncodingConv::switch_encoding($customer['name']);
		$customer['contact'] = EncodingConv::switch_encoding($customer['contact']);
		$customer['phone'] = EncodingConv::switch_encoding($customer['phone']);
		$customer['address'] = EncodingConv::switch_encoding($customer['address']);
		$customer['machine_model'] = EncodingConv::switch_encoding($customer['machine_model']);
		return $customer;
	}
	
	public function query_all()
	{
		$customer_list_result = $this->db_adapter->query('EXEC SP_Customer_GetDefaultOnes @set = 2', array());
		$customer_list = array();
		foreach ($customer_list_result as $customer)
		{
		    $customer['id'] = $customer['ID'];
			$customer['name'] = EncodingConv::switch_encoding($customer['name']);
			$customer['contact'] = EncodingConv::switch_encoding($customer['contact']);
			$customer['phone'] = EncodingConv::switch_encoding($customer['phone']);
			$customer['address'] = EncodingConv::switch_encoding($customer['address']);
			$customer['machine_model'] = EncodingConv::switch_encoding($customer['machine_model']);
			array_push($customer_list, $customer);
		}
		return $customer_list;
	}
	
	public function add_customer($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Customer_Add @name = ?, @creator = ?, @cookieNew = ?, @vip_level = ?, @contact = ?, @phone = ?, @address = ?, @distance = ?, @machine_model = ?',
			array(
				EncodingConv::switch_encoding_reverse($parameter['name']),
				$parameter['creator'],
				$parameter['cookieNew'],
				$parameter['vip_level'],
				EncodingConv::switch_encoding_reverse($parameter['contact']),
				EncodingConv::switch_encoding_reverse($parameter['phone']),
				EncodingConv::switch_encoding_reverse($parameter['address']),
				$parameter['distance'],
				EncodingConv::switch_encoding_reverse($parameter['machine_model'])
			));
		$id = $result->current()[""];
		if ($id == -1)
		    return array('result' => false, 'reason' => array(ErrorType::CUSTOMER_NAME_OCCUPIED));
		else
		  return array('result' => true, 'id' => $id);
	}
	
	public function edit_customer($parameter)
	{
		$result = $this->db_adapter->query(
			'EXEC SP_Customer_Edit @id = ?, @name = ?, @vip_level = ?, @contact = ?, @phone = ?, @address = ?, @distance = ?, @machine_model = ?',
			array(
				$parameter['id'], 
				EncodingConv::switch_encoding_reverse($parameter['name']), 
				$parameter['vip_level'], 
				EncodingConv::switch_encoding_reverse($parameter['contact']),
				EncodingConv::switch_encoding_reverse($parameter['phone']),
				EncodingConv::switch_encoding_reverse($parameter['address']),
				$parameter['distance'],
				EncodingConv::switch_encoding_reverse($parameter['machine_model'])
			));
		return array('result' => true);
	}
}

?>