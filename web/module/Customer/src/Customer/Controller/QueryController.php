<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Customer for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Customer\Util\ErrorType;
use Customer\Util\ControllerUtil;

class QueryController extends AbstractActionController
{
    private $auth;
    private $customer_service;

    public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
        \Customer\Db\CustomerService $customer_service,
    ) {
        $this->auth = $auth;
        $this->customer_service = $customer_service;
    }

    public function allAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['customer_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));

		$this->layout()->setTemplate('customer/layout/layout')->setVariables(array('navbar_active' => 'customer'));
		
        $customer_list = $this->customer_service->query_all();
        return array('customer_list' => $customer_list);
    }
    
    public function oneAction()
    {
		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['customer_operator'])
		    return ControllerUtil::generateErrorViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
        $id = $this->params('id');
        if (is_numeric($id))
        {
            $customer = $this->customer_service->query_by_id($id);
			$this->layout()->setTemplate('customer/layout/layout')->setVariables(array('navbar_active' => 'customer'));
            return array('customer' => $customer);
        }
        else
        {
            /* Rendering */
        
            $empty = array(
                'id' => 'new',
                'name' => '',
                'vip_level' => '0',
            	'contact' => null,
            	'phone' => null,
            	'address' => null,
            	'distance' => null,
            	'machine_model' => null,
            );
			$this->layout()->setTemplate('customer/layout/layout')->setVariables(array('navbar_active' => 'customer'));
            return array(
                'customer' => $empty,
            );
        }
    }
}
