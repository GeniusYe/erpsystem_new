<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Customer for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\InputFilter\Factory as FilterFactory;
use Customer\Util\ErrorType;
use Customer\Util\ControllerUtil;

class MaintainController extends AbstractActionController
{
	private $auth;
	private $customer_service;
	private $db_adapter;

	public function __construct(
        \Laminas\Authentication\AuthenticationService $auth,
        \Customer\Db\CustomerService $customer_service,
		\Laminas\Db\Adapter\Adapter $db_adapter,
	) {
		$this->auth = $auth;
		$this->customer_service = $customer_service;
		$this->db_adapter = $db_adapter;	
	}

    public function createAction()
	{
		if (!$this->getRequest()->isPost())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::POST_ONLY)));
		
		$factory = new FilterFactory();
		$filter = $factory->createInputFilter(array(
			'id' => array(),
			'name' => array( 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'vip_level' => array( 'validators' => array( array( 'name' => 'int' ) ) ),
			'contact' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'phone' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'address' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
			'distance' => array( 'required' => false, 'validators' => array( array( 'name' => 'int' ) ) ),
			'machine_model' => array( 'required' => false, 'filters' => array( array( 'name' => 'stringtrim' ) ) ),
		));

		$raw_data = $this->getRequest()->getPost();
		$filter->setData($raw_data);
		
		$result = false;

		if (!$filter->isValid())
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_NOT_VALID, $filter->getMessages())));

		$auth_status = ControllerUtil::checkAuthentication($this->auth);
		if ($auth_status['is_authenticated'] !== true)
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_AUTHENTICATED)));
		if (!$auth_status['customer_operator'])
		    return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::NOT_ENOUGH_RIGHTS)));
		
		$this->db_adapter->getDriver()->getConnection()->beginTransaction();
		
		/* Start Query */
				
		$id = $filter->getValue('id');
		
		if ($id === 'new') { }
		else if (is_numeric($id))
			$id = intval($id);
		else
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));

		/* Merge */
		
		if ($id === 'new')
		{
			$result = $this->customer_service->add_customer(array(
				'name' => $filter->getValue('name'),
				'vip_level' => $filter->getValue('vip_level'),
			    'creator' => $auth_status['user']['ID'],
			    'cookieNew' => 0,
				'contact' => $filter->getValue('contact'),
				'phone' => $filter->getValue('phone'),
				'address' => $filter->getValue('address'),
				'distance' => $filter->getValue('distance'),
				'machine_model' => $filter->getValue('machine_model'),
			));

			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			else
				$id = $result['id'];
			
			$result = true;
		}
		else if (is_numeric($id))
		{
			$result = $this->customer_service->edit_customer(
				array(
				    'id' => $id,
					'name' => $filter->getValue('name'),
				    'vip_level' => $filter->getValue('vip_level'),
				    'creator' => $auth_status['user']['ID'],
				    'cookieNew' => 0,
					'contact' => $filter->getValue('contact'),
					'phone' => $filter->getValue('phone'),
					'address' => $filter->getValue('address'),
					'distance' => $filter->getValue('distance'),
					'machine_model' => $filter->getValue('machine_model'),
				)
			);
			
			if ($result['result'] !== true)
				return ControllerUtil::generateAjaxViewModel($this->getResponse(), $result);
			
			$result = true;
		}
		else 
			return ControllerUtil::generateAjaxViewModel($this->getResponse(), array('result' => false, 'reason' => array(ErrorType::PARAMETER_OUT_OF_RANGE)));
	
		$this->db_adapter->getDriver()->getConnection()->commit();

		/* Rendering */
		
	   	return ControllerUtil::generateAjaxViewModel($this->getResponse(), array(
			'result' => $result
		));
	}
}
