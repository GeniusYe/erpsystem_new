<?php

namespace Customer\Controller\Factory;

use Interop\Container\ContainerInterface;
use Customer\Controller\MaintainController;

class MaintainControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new MaintainController(
            $container->get('auth'),
            $container->get('customer_service'),
            $container->get('database_adapter'),
        );
    }
}