<?php

namespace Customer\Controller\Factory;

use Interop\Container\ContainerInterface;
use Customer\Controller\QueryController;

class QueryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new QueryController(
            $container->get('auth'),
            $container->get('customer_service'),
        );
    }
}