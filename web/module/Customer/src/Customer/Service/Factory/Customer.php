<?php

namespace Customer\Service\Factory;

use Interop\Container\ContainerInterface;
use Customer\Db\CustomerService;

class Customer
{
	public function __invoke(ContainerInterface $container)
	{
		$db_adapter = $container->get('database_adapter');
		$customer_service = new CustomerService($db_adapter);
		return $customer_service;
	}
}

?>