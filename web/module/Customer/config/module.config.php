<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'Customer\Controller\Query' => Customer\Controller\QueryController::class,
			'Customer\Controller\Maintain' => Customer\Controller\MaintainController::class,
		),
		'factories' => array(
			Customer\Controller\QueryController::class => Customer\Controller\Factory\QueryControllerFactory::class,
			Customer\Controller\MaintainController::class => Customer\Controller\Factory\MaintainControllerFactory::class,
		)
	),
	'router' => array(
		'routes' => array(
			'customer' => array(
				'type'	=> 'Literal',
				'options' => array(
					// Change this to something specific to your module
					'route'	=> '/customer',
					'defaults' => array(
						// Change this value to reflect the namespace in which
						// the controllers for your module are found
						'__NAMESPACE__' => 'Customer\Controller',
						'controller'	=> 'QueryController',
						'action'		=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					// This route is a sane default when developing a module;
					// as you solidify the routes for your module, however,
					// you may want to remove it and replace it with more
					// specific routes.
					'default' => array(
						'type'	=> 'Segment',
						'options' => array(
							'route'	=> '/:controller/:action/[:id]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'	 => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'	     => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'customer_service' => Customer\Service\Factory\Customer::class,
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Customer' => __DIR__ . '/../view',
		),
		'template_map' => include __DIR__ . '/../template_map.php',
	),
);
