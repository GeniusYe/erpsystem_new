function recalculate()
{
	$.ajax({
		url: "../../calculate/index/" + $time_str,
		type: "POST",
		success: function()
		{
			showInfoDialog("重新计算成功", function() { window.location.reload(); });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
	event.preventDefault();
}

