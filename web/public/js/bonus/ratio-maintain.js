$(document).ready(function()
{
	$("#ratio-form").ajaxForm({
		type: "POST",
		success: function()
		{
			showInfoDialog("@success", function() { window.location.reload(); });
		},
		error: function(data)
		{
			showInfoDialog("@error " + data.responseJSON.reason);
		}
	});
});