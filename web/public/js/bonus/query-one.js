$(document).ready(function(){
	$("#additionDialog div.modal-footer button[name='confirm']").bind("click", function(){
		$modal_body = $("#additionDialog div.modal-body");
		$.ajax({
			url: "../../../addition/create/" + $time_str + "/" + $attendant_id,
			type: "POST",
			data: {
				id: $modal_body.find("input[name='id']").val(),
				bonus: $modal_body.find("input[name='bonus']").val(),
				reason: $modal_body.find("input[name='reason']").val(),
			},
			success: function()
			{
				window.location.reload();
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
		event.preventDefault();
	});
	
	set_print_bonus();
});

function add_addition()
{
	$bonus = 0;
	$reason = "";
	$("#additionDialog div.modal-body input[name='id']").val("new");
	$("#additionDialog div.modal-body input[name='bonus']").val($bonus);
	$("#additionDialog div.modal-body input[name='reason']").val($reason);
	$("#additionDialog").modal("show");
	event.preventDefault();
}

function modify_addition(id)
{
	$bonus = $("#addition_table tr.tr_" + id + " td[name='bonus']").text();
	$reason = $("#addition_table tr.tr_" + id + " td[name='reason']").text();
	$("#additionDialog div.modal-body input[name='id']").val(id);
	$("#additionDialog div.modal-body input[name='bonus']").val($bonus);
	$("#additionDialog div.modal-body input[name='reason']").val($reason);
	$("#additionDialog").modal("show");
}

function set_print_bonus()
{
	var $table_str = "";
	$table_str += "<h4 class='text-center print-title'>" + $("#title").text() + "</h4>";
	$table_str += "<table id='data_table_print_voucher' class='data_table_print'><thead>" + $("#voucher_table thead").html() + "</thead><tbody>";
	$table_str += $("#voucher_table tbody").html();
	$table_str += "</tbody></table><h6>&nbsp;</h6>";
	$table_str += "<table id='data_table_print_addition' class='data_table_print'><thead>" + $("#addition_table thead").html() + "</thead><tbody>";
	$table_str += $("#addition_table tbody").html();
	$table_str += "</tbody></table><h6>&nbsp;</h6>";
	$table_str += "<table id='data_table_print_total' class='data_table_print'><thead>" + $("#total_table thead").html() + "</thead><tbody>";
	$table_str += $("#total_table tbody").html();
	$table_str += "</tbody></table><h6>&nbsp;</h6>";
	$table_str += "<div class='print-footer'>" +
			"提成受益人签名：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			"领导审核：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			"</div>";
	$("#printDiv").html($table_str);
}

function print_bonus()
{
	window.print();
}