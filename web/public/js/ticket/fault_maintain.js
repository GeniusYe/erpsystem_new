$(document).ready(function()
{
	$("#ticket_fault_form a[name='submit']").bind("click", function()
	{
		$("#ticket_fault_form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.href = "../all/"; });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
});
