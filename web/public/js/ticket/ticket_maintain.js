$(document).ready(function()
{
	if ($ticket_state == 0)
	{
		$("#ticket_form input, #ticket_form select, #ticket_form textarea, #ticket_form button").attr("readonly", false);
		$("#ticket_form textarea[name='remaining_fault']").attr("readonly", true);
	}
	else if ($ticket_state == 1)
	{
		$("#ticket_form input[name='customer_name'], #ticket_form input[name='contact'], #ticket_form input[name='phone'], " +
				"#ticket_form input[name='address'], #ticket_form input[name='distance'], " +
				"#ticket_form select[name='attendant_id'], #ticket_form select[name='truck']")
			.attr("readonly", true);
	}
	else
	{
		$("#ticket_form input, #ticket_form select, #ticket_form textarea, #ticket_form button").attr("readonly", true);
		$("#ticket_form tr.submit").css("display", "none");
	}
	
	/* Customer Name */
	
	$("#ticket_form input[name='customer_name']").typeahead({ source : customer_name_list });
	$("#ticket_form input[name='customer_name']").bind("change", function()
	{
		var $val = $(this).val();
		var i;
		for (i = 0; i < customer_list.length; i++)
		{
			if (customer_list[i].name == $val)
			{
				var $customer = customer_list[i];
				$("#ticket_form input[name='customer_id']").val($customer.id);
				$("#ticket_form input[name='contact']").val($customer.contact);
				$("#ticket_form input[name='phone']").val($customer.phone);
				$("#ticket_form input[name='address']").val($customer.address);
				$("#ticket_form input[name='distance']").val($customer.distance);
				$("#ticket_form input[name='machine_model']").val($customer.machine_model);
				break;
			}
		}
		if (i == customer_list.length)
		{
			$("#ticket_form input[name='customer_id']").val('');
			$("#ticket_form input[name='customer_name']").val('');
			$("#ticket_form input[name='contact']").val('');
			$("#ticket_form input[name='phone']").val('');
			$("#ticket_form input[name='address']").val('');
			$("#ticket_form input[name='distance']").val('');
			$("#ticket_form input[name='machine_model']").val('');
		}
	});
	
	/* Solved Fault */
	
	$("#ticket_form textarea[name='solved_fault']").bind("focus", function(){
		$("#fault_div").animate({"right" : "-20px" });
	});
	$("#ticket_form input, #ticket_form select, #ticket_form button, #ticket_form textarea[name!='solved_fault']").bind("focus", function(){
		$("#fault_div").animate({"right" : "-20%" });
	});
	$("#ticket_form a").bind("click", function(){
		$("#fault_div").animate({"right" : "-20%" });
	});
	
	$("#fault_div ul > li").bind("click", function(){
		var $fault = $(this).text();
		$("#ticket_form textarea[name='solved_fault']").val($("#ticket_form textarea[name='solved_fault']").val() + $fault + "\n");
	});
	
	$("#form_search_input").bind("keyup", function(){
		var $name = $(this).val();
		$("#fault_div ul li").removeClass("hidden");
		$.each($("#fault_div ul li"), function(){
			if ($(this).text().indexOf($name, 0) == -1)
			{
				$(this).addClass("hidden");
			}
		});
	});
	
	/* Submit */
	
	$("#ticket_form a[name='submit']").bind("click", function()
	{
		$("#ticket_form").ajaxSubmit(
		{
			beforeSubmit: function()
			{
				showInfoDialog("@operating");
			},
			success: function(data)
			{
				showInfoDialog("@success", function() { window.history.go(-1); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
});
