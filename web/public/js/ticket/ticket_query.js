$(document).ready(function()
{
});

function modify_ticket(id)
{
	window.location.href = "../one/" + id;
}

function cancel_ticket(id)
{
	showConfirmDialog("您正在尝试作废本张派工单，派工单作废后无法恢复，您确认要这样做吗？", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../../maintain/cancel/" + id,
			type: "POST",
			dataType: "json",
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.reload(); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function copy_ticket(id)
{
	showConfirmDialog("您正在尝试复制一张派工单，复制产生的派工单和原派工单保持一致，该操作无法撤销，如果失误操作您将只能作废派工单，您确认要这样做吗？", function()
	{
		showInfoDialog("@operating");
		$.ajax({
			url: "../../maintain/copy/" + id,
			type: "POST",
			dataType: "json",
			success: function(data)
			{
				showInfoDialog("@success", function() { window.location.reload(); });
			},
			error: function(data)
			{
				showInfoDialog("@error " + data.responseJSON.reason);
			}
		});
	});
}

function add_ticket()
{
	window.location.href = "../one/";
}

function print_ticket(id)
{
	window.open("../../maintain/print/" + id);
}

