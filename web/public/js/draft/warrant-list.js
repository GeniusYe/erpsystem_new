function updateCheckedValue() {
	$checkbox_values = {};
	$("#warrant-list-table :checkbox").each(function(){
    	$checkbox_values[this.id] = this.checked;
    });
    sessionStorage.setItem("warrant-list.checkbox_values", JSON.stringify($checkbox_values));
}

$(function(){
	$("#submit").bind("click", function(){
		var id_list = [];
        $.each($("input[name='warrant_id']:checked"), function(){
        	id_list.push($(this).val());
        });
        window.location.href = "../query/query?id_list=" + id_list.join(",");
	});
	$("a.select-all").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-none").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		updateCheckedValue();
	});
	$("a.select-all-sale").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_sale").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-all-sale-w-w").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_sale_w_w").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-all-sale-wo-w").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_sale_wo_w").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-all-service").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_service").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-all-gift").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_gift").prop('checked', true);
		updateCheckedValue();
	});
	$("a.select-all-adjustment").bind("click", function(){
		$("input[name='warrant_id']").prop('checked', false);
		$("input[name='warrant_id'].warrant_adjustment").prop('checked', true);
		updateCheckedValue();
	});
	
	var $checkbox_values = JSON.parse(sessionStorage.getItem('warrant-list.checkbox_values')) || {};
	$.each($checkbox_values, function(key, value) {
	  $("#" + key).prop('checked', value);
	});
	
	$("#warrant-list-table :checkbox").on("change", function(){
		updateCheckedValue();
	});
});