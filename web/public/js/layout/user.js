$(document).ready(function()
{
	$.ajax({
		url: $base_path + "/authentication/log/status/",
		type: "POST",
		dataType: "json",
		data: { },
		success: function(data)
		{
			if (data.user == null)
			{
				$("#navbar > ul.navbar-right > li[name='login']").fadeIn();
			}
			else
			{
				$("#navbar > ul.navbar-right > li[name='logout']").fadeIn();
				$("#navbar > ul > li.authenticated").fadeIn();
				$("#navbar > ul > li.not_authenticated").fadeOut();
			}
		}
	});
});